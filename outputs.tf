 output "bastion_fqdn" {
    value = azurerm_bastion_host.nucor_bastion.dns_name
}

output "bastion_id" {
    value = azurerm_bastion_host.nucor_bastion.id
}

output "vnet_name" {
    value = azurerm_virtual_network.nucor_vnet.name
}

output "vnet_location" {
    value = azurerm_virtual_network.nucor_vnet.location
}

output "resource_group_name" {
    value = azurerm_resource_group.nucor_resource_group.name
}

output "resource_group_location" {
    value = azurerm_resource_group.nucor_resource_group.location
}

output "lb_id" {
    value = azurerm_lb.nucor_lb.id
}