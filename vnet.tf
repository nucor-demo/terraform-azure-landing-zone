resource "azurerm_virtual_network" "nucor_vnet" {
  name                = var.virtual_network_name
  location            = azurerm_resource_group.nucor_resource_group.location
  resource_group_name = azurerm_resource_group.nucor_resource_group.name
  address_space       = var.vnet_address_space
  dns_servers         = var.vnet_dns_servers
  bgp_community       = var.vnet_bgp_community

  dynamic "ddos_protection_plan" {
    for_each = var.vnet_ddos_protection_plan == null ? {} : var.vnet_ddos_protection_plan

    content {
      id     = var.vnet_ddos_protection_plan.id
      enable = var.vnet_ddos_protection_plan.enable
    }
  }

  tags = var.vnet_tags
}
