resource "azurerm_public_ip" "nucor_lb_public_ip" {
  name                = var.lb_public_ip_name
  location            = azurerm_resource_group.nucor_resource_group.location
  resource_group_name = azurerm_resource_group.nucor_resource_group.name
  allocation_method   = "Static"
}

resource "azurerm_lb" "nucor_lb" {
  name                = var.lb_name
  location            = azurerm_resource_group.nucor_resource_group.location
  resource_group_name = azurerm_resource_group.nucor_resource_group.name

  frontend_ip_configuration {
    name                 = var.lb_public_ip_name
    public_ip_address_id = azurerm_public_ip.nucor_lb_public_ip.id
  }
}
