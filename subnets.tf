resource "azurerm_subnet" "nucor_subnet" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.nucor_resource_group.name
  virtual_network_name = azurerm_virtual_network.nucor_vnet.name
  address_prefixes     = [var.bastion_subnet_cidr]

  depends_on = [azurerm_virtual_network.nucor_vnet]
}
