variable "nucor_subnets" {
  type = map(object({
    name             = string
    address_prefixes = list(string)
    delegation_block = optional(map(object({
      name = string
      service_delegation = map(object({
        name    = string
        actions = optional(list(string))
      }))
    })))
  }))
  default = null
}

variable "subnet_tags" {
  type = map(any)
  default = null
}

variable "vnet_tags" {
  type = map(any)
  default = null
}

variable "vnet_dns_servers" {
  type = list(string)
  default = null
}

variable "vnet_address_space" {
  type = list(string)
}

variable "vnet_bgp_community" {
  type = string
  default = null
}

variable "vnet_ddos_protection_plan" {
  type = map(object({
    id     = string
    enable = bool
  }))
  default = null
}

variable "lb_name" {
  type = string
}

variable "lb_public_ip_name" {
  type = string
}

variable "bastion_public_ip_name" {
  type = string
}

variable "bastion_name" {
  type = string
}

variable "bastion_subnet_cidr" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "resource_group_location" {
  type = string
}

variable "virtual_network_name" {
  type = string
}
