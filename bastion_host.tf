resource "azurerm_public_ip" "nucor_bastion_public_ip" {
  name                = var.bastion_public_ip_name
  location            = azurerm_resource_group.nucor_resource_group.location
  resource_group_name = azurerm_resource_group.nucor_resource_group.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_bastion_host" "nucor_bastion" {
  name                = var.bastion_name
  location            = azurerm_resource_group.nucor_resource_group.location
  resource_group_name = azurerm_resource_group.nucor_resource_group.name

  ip_configuration {
    name                 = "bastion_ip_configuration"
    subnet_id            = azurerm_subnet.nucor_subnet.id
    public_ip_address_id = azurerm_public_ip.nucor_bastion_public_ip.id
  }
}
